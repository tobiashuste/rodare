# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Zenodo schema.org serializer."""

from __future__ import absolute_import, print_function

from zenodo.modules.records.models import ObjectType
from zenodo.modules.records.serializers.schemas import schemaorg as schemas

from .json import ZenodoJSONSerializer


class ZenodoSchemaOrgSerializer(ZenodoJSONSerializer):
    """Zenodo schema.org serializer.

    Serializes the record using the appropriate marshmallow schema based on
    its schema.org type.
    """

    @classmethod
    def _get_schema_class(self, obj):
        data = obj['metadata']
        obj_type = ObjectType.get_by_dict(data['resource_type'])
        return getattr(schemas, obj_type['schema.org'][19:])

    def dump(self, obj, context=None):
        """Serialize object with schema."""
        # Resolve string "https://schema.org/ScholarlyArticle"
        # to schemas.ScholarlyArticle class (etc.)
        schema_cls = self._get_schema_class(obj)
        return schema_cls(context=context).dump(obj).data
