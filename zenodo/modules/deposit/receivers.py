# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Zenodo Deposit module receivers."""

from __future__ import absolute_import, print_function

from flask import current_app
from flask_security import current_user
from invenio_pidrelations.contrib.versioning import PIDVersioning
from invenio_sipstore.models import RecordSIP
from rodare_robis.tasks import publish_in_robis, register_id_in_robis, \
    update_after_publish

from zenodo.modules.deposit.tasks import datacite_register
from zenodo.modules.openaire.tasks import openaire_direct_index
from zenodo.modules.sipstore.tasks import archive_sip


def datacite_register_after_publish(sender, action=None, pid=None,
                                    deposit=None):
    """Mind DOI with DataCite after the deposit has been published."""
    if action == 'publish' and \
            current_app.config['DEPOSIT_DATACITE_MINTING_ENABLED']:
        recid_pid, record = deposit.fetch_published()
        datacite_register.delay(recid_pid.pid_value, str(record.id))


def openaire_direct_index_after_publish(sender, action=None, pid=None,
                                        deposit=None):
    """Send published record for direct indexing at OpenAIRE."""
    if current_app.config['OPENAIRE_DIRECT_INDEXING_ENABLED']:
        _, record = deposit.fetch_published()
        if action in 'publish':
            openaire_direct_index.delay(record_uuid=str(record.id))


def sipstore_write_files_after_publish(sender, action=None, pid=None,
                                       deposit=None):
    """Send the SIP for archiving."""
    if action == 'publish' and \
            current_app.config['SIPSTORE_ARCHIVER_WRITING_ENABLED']:
        recid_pid, record = deposit.fetch_published()
        sip = (
            RecordSIP.query
            .filter_by(pid_id=recid_pid.id)
            .order_by(RecordSIP.created.desc())
            .first().sip
        )
        archive_sip.delay(str(sip.id))


def register_doi_in_robis(sender, action=None, pid=None,
                          deposit=None):
    """Register DOI in ROBIS after publishing."""
    if action == 'publish' and \
            current_app.config['ZENODO_ACTIVATE_ROBIS_INTEGRATION']:
        if current_user and current_user.is_authenticated:
            if current_user.profile:
                _, record = deposit.fetch_published()
                doi = record['conceptdoi']
                username = current_user.profile.username
                publish_in_robis.delay(
                    doi, username, record['pub_id']
                )


def trigger_update_in_robis(sender, action=None, pid=None, deposit=None):
    """Trigger update in ROBIS after publishing."""
    if action == 'publish' and \
            current_app.config['ZENODO_ACTIVATE_ROBIS_INTEGRATION']:
        pid, record = deposit.fetch_published()
        pv = PIDVersioning(child=pid)
        if pv.is_last_child:
            update_after_publish.delay(record['pub_id'])
