# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Bundles for Zenodo deposit form."""

from flask_assets import Bundle
from invenio_assets import NpmBundle
from invenio_deposit.bundles import js_dependecies_autocomplete, \
    js_dependecies_schema_form, js_dependecies_uploader, \
    js_dependencies_ckeditor, js_dependencies_jquery, \
    js_dependencies_ui_sortable, js_main

js_zenodo_deposit = Bundle(
    'js/zenodo_deposit/filters.js',
    'js/zenodo_deposit/directives.js',
    'js/zenodo_deposit/controllers.js',
    'js/zenodo_deposit/providers.js',
    'js/zenodo_deposit/config.js',
    depends=(
        'js/zenodo_deposit/*.js',
    ),
)


js_deposit = NpmBundle(
    js_dependencies_jquery,
    js_main,
    js_dependecies_uploader,
    js_dependecies_schema_form,
    js_dependecies_autocomplete,
    js_dependencies_ui_sortable,
    js_dependencies_ckeditor,
    js_zenodo_deposit,
    filters='uglifyjs',
    output='gen/rodare.deposit.%(version)s.js',
)
