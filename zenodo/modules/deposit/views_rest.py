# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Redirects for legacy URLs."""

from __future__ import absolute_import, print_function

import re

from flask import Blueprint, jsonify, request
from flask_security import current_user, login_required
from rodare_robis.harvester.robis_import import get_robis_publication

from .utils import suggest_language

blueprint = Blueprint(
    'zenodo_deposit',
    __name__,
    url_prefix='',
)


@blueprint.route(
    '/language/',
    methods=['GET']
)
@login_required
def language():
    """Suggest a language on the deposit form."""
    q = request.args.get('q', '')
    limit = int(request.args.get('limit', '5').lower())
    langs = suggest_language(q, limit=limit)
    langs = [{'code': l.alpha_3, 'name': l.name} for l in langs]
    d = {
        'suggestions': langs
    }
    return jsonify(d)


@blueprint.route(
    '/record/robis_import/<pub_id>/<doc_id>',
    methods=['POST']
)
@login_required
def robis_importmetadata(pub_id, doc_id=1):
    """Import metadata from Robis."""
    docid_re = re.compile('^[1-9]$')
    pubid_re = re.compile('^[1-9]*[1-9][0-9]*$')
    if not pubid_re.match(pub_id):
        data = dict(message='Publication ID does not have the correct format.')
        response = jsonify(data)
        response.status_code = 400
        return response
    elif not docid_re.match(doc_id):
        data = dict(message='Document ID does not have the correct format.')
        response = jsonify(data)
        response.status_code = 400
        return response
    username = ''
    if current_user.profile:
        username = current_user.profile.username
    metadata = get_robis_publication(
        pub_id,
        doc_id,
        only_published=False,
        username=username
    )
    if metadata:
        if not metadata['published']:
            return jsonify(metadata)
        data = dict(message='Your upload is already published in ROBIS.')
        status_code = 403
    else:
        data = dict(message='The Metadata could not be retrieved.')
        status_code = 404
    response = jsonify(data)
    response.status_code = status_code
    return response
