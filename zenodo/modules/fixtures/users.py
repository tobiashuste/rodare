# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Zenodo communities fixture loading."""

from __future__ import absolute_import, print_function

from flask import current_app
from flask_security.utils import hash_password
from invenio_access.models import ActionUsers
from invenio_accounts.models import User
from invenio_db import db


def loaduser(user_data):
    """Load a single user to Zenodo from JSON fixture."""
    kwargs = {
        'email': user_data['email'],
        'password': hash_password(user_data['password']),
        'active': user_data.get('active', True)
    }

    datastore = current_app.extensions['security'].datastore
    datastore.create_user(**kwargs)
    db.session.commit()
    user = User.query.filter_by(email=user_data['email']).one()
    actions = current_app.extensions['invenio-access'].actions
    actionusers_f = {
        'allow': ActionUsers.allow,
        'deny': ActionUsers.deny,
    }
    # e.g. [('allow', 'admin-access'), ]
    for action_type, action_name in user_data.get('access', []):
        action = actions[action_name]
        db.session.add(
            actionusers_f[action_type](action, user_id=user.id)
        )
    db.session.commit()
