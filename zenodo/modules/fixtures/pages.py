# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Pages fixtures."""

from __future__ import absolute_import, print_function

from os.path import join

from invenio_db import db
from invenio_pages.models import Page

from .utils import file_stream, read_json


def loadpages(force=False):
    """Load pages."""
    data = read_json('data/pages.json')

    try:
        if force:
            Page.query.delete()
        for p in data:
            if len(p['description']) >= 200:
                raise ValueError(  # pragma: nocover
                    "Description too long for {0}".format(p['url']))
            p = Page(
                url=p['url'],
                title=p['title'],
                description=p['description'],
                content=file_stream(
                    join('data', p['file'])).read().decode('utf8'),
                template_name=p['template_name'],
            )
            db.session.add(p)
        db.session.commit()
    except Exception:
        db.session.rollback()
        raise
