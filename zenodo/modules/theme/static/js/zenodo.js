// Copyright (C) 2017-2018 HZDR
//
// This file is part of Rodare.
//
// Rodare is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rodare is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rodare.  If not, see <http://www.gnu.org/licenses/>.
require([
  "jquery",
  "clipboard",
  "bootstrap",
  "typeahead",
  "bloodhound",
  "node_modules/angular/angular",
  "node_modules/angular-sanitize/angular-sanitize",
  "node_modules/angular-loading-bar/build/loading-bar",
  "node_modules/invenio-csl-js/dist/invenio-csl-js",
  "node_modules/bootstrap-switch/dist/js/bootstrap-switch",
  "js/zenodo/module",
  "js/zenodo/functions",
  "js/github/view",
  ], function($, Clipboard, recordCommunityCurate) {
    // On document ready bootstrap angular
    new Clipboard('.btn.clip-button');
  }
);
