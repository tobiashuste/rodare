# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Test query-based OAISet updating."""

from __future__ import absolute_import, print_function

from datetime import datetime

from invenio_records.api import Record

from zenodo.modules.utils.tasks import update_search_pattern_sets


def make_rec(comm, sets):
    """Create a minimal record for Community-OAISet testing."""
    return {'communities': comm, '_oai': {'sets': sets}}


def test_oaiset_update(app, db, oaisets, es, oaiset_update_records):
    """Test query-based OAI sets updating."""
    rec_ok, rec_rm, rec_add = [Record.get_record(uuid) for uuid
                               in oaiset_update_records]
    year_now = str(datetime.now().year)
    # Assume starting conditions
    assert set(rec_ok['_oai']['sets']) == set(['extra', 'user-foobar', ])
    assert rec_ok['_oai']['updated'].startswith('1970')
    assert set(rec_rm['_oai']['sets']) == set(['extra', 'user-foobar', ])
    assert rec_rm['_oai']['updated'].startswith('1970')
    assert set(rec_add['_oai']['sets']) == set(['user-foobar', ])
    assert rec_add['_oai']['updated'].startswith('1970')

    # Run the updating task
    update_search_pattern_sets.delay()

    rec_ok, rec_rm, rec_add = [Record.get_record(uuid) for uuid
                               in oaiset_update_records]
    # After update
    assert set(rec_ok['_oai']['sets']) == set(['extra', 'user-foobar', ])
    assert rec_ok['_oai']['updated'].startswith('1970')
    assert set(rec_rm['_oai']['sets']) == set(['user-foobar', ])
    assert rec_rm['_oai']['updated'].startswith(year_now)
    assert set(rec_add['_oai']['sets']) == set(['extra', 'user-foobar', ])
    assert rec_add['_oai']['updated'].startswith(year_now)
