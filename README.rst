..
    Copyright (C) 2017-2018 HZDR

    This file is part of Rodare.

    Rodare is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rodare is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rodare.  If not, see <http://www.gnu.org/licenses/>.


.. image:: https://gitlab.hzdr.de/rodare/rodare/badges/master/pipeline.svg
   :target: https://gitlab.hzdr.de/rodare/rodare/commits/master
   :alt: Pipeline status

.. image:: https://gitlab.hzdr.de/rodare/rodare/badges/master/coverage.svg
   :target: https://gitlab.hzdr.de/rodare/rodare/commits/master
   :alt: Coverage report

.. image:: https://img.shields.io/badge/License-GPL%20v3-blue.svg
   :target: https://www.gnu.org/licenses/gpl-3.0
   :alt: GPL-3.0

.. image:: https://img.shields.io/uptimerobot/status/m780212509-861052e21fa7e5c6606a6f74.svg
    :alt: Uptime Robot status
    :target: https://stats.uptimerobot.com/1GZYzH7Qy

.. image:: https://img.shields.io/uptimerobot/ratio/m780212509-861052e21fa7e5c6606a6f74.svg
    :alt: Uptime Robot ratio
    :target: https://stats.uptimerobot.com/1GZYzH7Qy

.. image:: https://img.shields.io/badge/Chat-Mattermost-blue.svg
    :alt: Join Mattermost
    :target: https://mattermost.hzdr.de/rodare/

Rodare - Rossendorf Data Repository
===================================

Rodare is an `HZDR <https://www.hzdr.de>`_ service provided by the Department
of Information Services and Computing. Rodare enables HZDR researchers to share
and preserve their research output in any size, any format and from any science
with other researchers from all over the planet.

The production instance of Rodare is reachable via https://rodare.hzdr.de
The staging environment can be used for testing: https://rodare-test.hzdr.de

Powered by Invenio
------------------
Rodare is a small layer on top of
`Invenio <http://github.com/inveniosoftware/invenio>`_, a ​free software
suite enabling you to run your own ​digital library or document repository on
the web.

Forked from Zenodo
------------------
Rodare is a fork of the popular data publication platform `Zenodo <https://zenodo.org>`_.


License
=======
Copyright (C) 2017-2018 HZDR

This file is part of Rodare.

Rodare is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Rodare is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Rodare.  If not, see <http://www.gnu.org/licenses/>.
