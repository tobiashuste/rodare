// Copyright (C) 2017-2018 HZDR
//
// This file is part of Rodare.
//
// Rodare is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rodare is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rodare.  If not, see <http://www.gnu.org/licenses/>.
require([
  "node_modules/invenio-search-js/dist/invenio-search-js",
  "node_modules/angular-strap/dist/modules/dimensions",
  "node_modules/angular-strap/dist/modules/compiler",
  "node_modules/angular-strap/dist/modules/tooltip",
  "node_modules/angular-strap/dist/modules/tooltip.tpl",
  "js/invenio_communities/module",
  ], function() {
  // On document ready bootstrap angular
  }
);
