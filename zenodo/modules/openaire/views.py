# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Blueprint for OpenAIRE."""

from __future__ import absolute_import, print_function

from flask import Blueprint

from .helpers import is_openaire_dataset, is_openaire_publication, \
    openaire_link

blueprint = Blueprint(
    'zenodo_openaire',
    __name__
)


@blueprint.app_template_filter('is_openaire_publication')
def is_publication(record):
    """Test if record is an OpenAIRE publication."""
    return is_openaire_publication(record)


@blueprint.app_template_filter('is_openaire_dataset')
def is_dataset(record):
    """Test if record is an OpenAIRE dataset."""
    return is_openaire_dataset(record)


@blueprint.app_template_filter('openaire_link')
def link(record):
    """Generate an OpenAIRE link."""
    return openaire_link(record)
