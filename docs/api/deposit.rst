..
    Copyright (C) 2017-2018 HZDR

    This file is part of Rodare.

    Rodare is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rodare is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rodare.  If not, see <http://www.gnu.org/licenses/>.


Deposit
-------

.. automodule:: zenodo.modules.deposit
   :members:
   :undoc-members:

API
~~~

.. automodule:: zenodo.modules.deposit.api
   :members:
   :undoc-members:


Loaders
~~~~~~~

.. automodule:: zenodo.modules.deposit.loaders
   :members:
   :undoc-members:


.. automodule:: zenodo.modules.deposit.loaders.base
   :members:
   :undoc-members:


Minters and fetchers
~~~~~~~~~~~~~~~~~~~~

.. automodule:: zenodo.modules.deposit.fetchers
   :members:
   :undoc-members:

.. automodule:: zenodo.modules.deposit.minters
   :members:
   :undoc-members:

Indexing
~~~~~~~~
.. automodule:: zenodo.modules.deposit.indexer
   :members:
   :undoc-members:


Search
~~~~~~
.. automodule:: zenodo.modules.deposit.query
   :members:
   :undoc-members:


Views
~~~~~
.. automodule:: zenodo.modules.deposit.views
   :members:
   :undoc-members:

Errors
~~~~~~

.. automodule:: zenodo.modules.deposit.errors
   :members:
   :undoc-members:
