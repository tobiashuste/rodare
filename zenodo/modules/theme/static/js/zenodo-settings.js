// Copyright (C) 2017-2018 HZDR
//
// This file is part of Rodare.
//
// Rodare is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rodare is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

require.config({
  baseUrl: "/static/",
  paths: {
    jquery: "node_modules/jquery/jquery",
    bootstrap: "node_modules/bootstrap-sass/assets/javascripts/bootstrap",
    angular: "node_modules/angular/angular",
    typeahead: 'node_modules/typeahead.js/dist/typeahead.jquery',
    bloodhound: 'node_modules/typeahead.js/dist/bloodhound',
    clipboard: 'node_modules/clipboard/dist/clipboard',
  },
  shim: {
    angular: {
      exports: 'angular'
    },
    jquery: {
      exports: "$"
    },
    bootstrap: {
      deps: ["jquery"]
    },
    clipboard: {
      exports: "Clipboard"
    }
  }
});
