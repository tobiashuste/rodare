..
    Copyright (C) 2017-2018 HZDR

    This file is part of Rodare.

    Rodare is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rodare is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rodare.  If not, see <http://www.gnu.org/licenses/>.


.. include:: ../README.rst
   :end-before: Powered by Invenio

**Powered by Invenio**: Rodare is a small layer on top of
`Invenio <http://inveniosoftware.org>`_, a free software suite enabling you
to run your own digital repository on the web. It is a fork of the data publication
platform `Zenodo <https://zenodo.org>`_.

.. note::
   Zenodo (R) and the Zenodo logo are trademarked by CERN and are not covered
   by the GPLv2 license. Thus, if you instantiate Zenodo on your own servers
   you must change the branding to your own.

   Please consider creating your own overlay to
   `Invenio <http://inveniosoftware.org>`_ and look to Zenodo for inspiration.
   We will be happy to help you get started, just contact us on
   info@zenodo.org.


Developers's Guide
------------------

This part of the documentation will show you how to get started in running Rodare locally.

.. toctree::
   :maxdepth: 2

   installation
   usage

API Reference
-------------

If you are looking for information on a specific function, class or method,
this part of the documentation is for you.

.. toctree::
   :maxdepth: 2

   api/applications
   api/config
   api/deposit
   api/fixtures
   api/records
   api/theme

Project documentation
---------------------

This part of the documentation describes Rodare project management and
development processes.

.. toctree::
   :maxdepth: 2

   development/guide
   development/process
   development/projectlifecycle

Additional Notes
----------------

Notes on how to contribute, legal information and changes are here for the
interested.

.. toctree::
   :maxdepth: 1

   contributing
   changes
   license
