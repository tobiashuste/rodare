# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

from fabric.api import *

# user to use for the remote commands
env.user = 'root'


def qa():
    """QA specific environment."""
    env.hosts = ["rodare-test.hzdr.de"]


def production():
    """QA specific environment."""
    env.hosts = ["rodare1.fz-rossendorf.de",
                 "rodare2.fz-rossendorf.de", "rodare3.fz-rossendorf.de"]


@parallel
def update_repo(version=None):
    """
    Update repo to latest production tag.
    """
    if not version:
        puts('Specify a version.')
        return
    with cd('/home/rodare/rodare'):
        run('su rodare -c \'git fetch origin\'')
        run('su rodare -c \'git reset --hard HEAD\'')
        puts('Discarded all untracked and modified files')
        run('su rodare -c \'git checkout {version}\''.format(version=version))
        puts('Checked out {version}.'.format(version=version))


@parallel
def deploy():
    """
    Deploy rodare.

    Installs requirements from requirements.txt.
    Install npm dependencies and builds assets.
    """
    with cd('/home/rodare/rodare'):
        with shell_env(INVENIO_INSTANCE_PATH='/opt/rodare/virtualenv/rodare/var/instance',
                       VIRTUAL_ENV='/opt/rodare/virtualenv/rodare'):
            with prefix('source /opt/rodare/virtualenv/rodare/bin/activate'):
                puts('Install python requirements and Rodare:')
                run('pip install -r requirements.txt')
                run('pip install -e .[postgresql,elasticsearch2]')
                run('python -O -m compileall .')
                puts('Successfully installed python requirements and Rodare.')
                puts('Install npm dependencies:')
                run('zenodo npm --pinned-file package.pinned.json')
                run('cd ${INVENIO_INSTANCE_PATH}/static && npm install')
                run('zenodo collect -v')
                run('zenodo assets build')
                puts('Successfully installed npm dependencies.')
                run('chown -R rodare:rodare /opt/rodare/virtualenv/rodare/')


@runs_once
def fixtures():
    """
    Install and load fixtures.
    """
    with shell_env(INVENIO_INSTANCE_PATH='/opt/rodare/virtualenv/rodare/var/instance',
                   VIRTUAL_ENV='/opt/rodare/virtualenv/rodare'):
        with prefix('source /opt/rodare/virtualenv/rodare/bin/activate'):
            run('zenodo fixtures loadpages --force')


@parallel
def update_mathjax(version=None):
    """
    Update local MathJax to specific version.
    """
    if not version:
        puts('Please specify the version you wish to install.')
        return
    with cd('/opt/rodare/virtualenv/rodare/var/instance/static/MathJax'):
        run('git fetch origin')
        run('git reset --hard HEAD')
        run('git checkout {version}'.format(version=version))
        run('chown -R rodare:rodare /opt/rodare/virtualenv/rodare/var/instance/static/MathJax')


def restart_rodare():
    """
    Restart rodare services on all machines.
    """
    run('service rodare reload')
    run('service rodare-worker restart')


@parallel
def apt_upgrade():
    """
    Run apt-get upgrade on all machines.
    """
    pass
