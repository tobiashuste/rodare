# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

""""Simple script to make an upload via the REST API."""

from __future__ import absolute_import, print_function

import json
import os.path
import subprocess
from time import sleep
from urllib.parse import urlparse

import requests


def get_bucket_id(bucket_link):
    """
    Extract bucket id from the API link.

    Args:
        bucket_link: Link to the bucket API endpoint.

    Returns:
        The extracted bucket_id.
    """
    path = urlparse(bucket_link).path
    return os.path.split(path)[1]


def upload_files(path, bucket_link):
    """
    Upload files without HTTP request.

    The function uses the CLI of invenio-files-rest.

    Args:
        path: The path to the files or folder to be uploaded.
        bucket_link: Link to the bucket API endpoint.
    """
    # Extract destination bucket id from bucket link
    bucket_id = get_bucket_id(bucket_link)
    print('Copy files...')
    try:
        # use command line interface to upload local file
        subprocess.check_output(
            ['zenodo', 'files', 'bucket', 'cp', path, bucket_id]
        )
    except subprocess.CalledProcessError:
        print("Upload of files failed. Upload incomplete.")


def upload(token, metadata, path, publish=True):
    """Make an upload."""
    base_url = 'https://149.220.36.41/api/deposit/depositions'
    auth = {
        'Authorization': 'Bearer {0}'.format(token)
    }
    auth_json = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
    }
    auth_json.update(auth)

    r = requests.post(base_url, data='{}', headers=auth_json, verify=False)
    assert r.status_code == 201
    links = r.json()['links']
    print('Create deposit:')
    print(r.json())
    # Wait for ES to index.
    sleep(1)

    upload_files(path, links['bucket'])

    r = requests.put(
        links['self'],
        data=json.dumps(dict(metadata=metadata)),
        headers=auth_json,
        verify=False
    )
    assert r.status_code == 200
    print('Update metadata:')
    print(r.json())

    if publish:
        r = requests.post(links['publish'], headers=auth, verify=False)
        assert r.status_code == 202
        print('Publish:')
        print(r.json())

    return r.json()['id']


def upload_test(token, path, publish=True):
    """
    Make upload to data management directly from the commandline.

    When calling this function a valid token needs to be passed to
    authenticate successfully.

    Args:
        token: The generated authentication token for the API access.
        path: Either a path to a folder or a path to a file.
        publish: if True, the publication is published

    Returns:
        int: The id of the new publication.
    """
    # Disable warnings about invalid certificate.
    # Remove verify=False in production
    requests.packages.urllib3.disable_warnings()

    metadata = {
        'title': 'Bika Lims Virtual Box Image',
        'upload_type': 'dataset',
        'description': 'Ubuntu 16.04 installation of BIKA Lims in VirtualBox.',
        'access_right': 'open',
        'license': 'cc-by',
        'creators': [{'name': 'Frust, Tobias', 'affiliation': 'HZDR'}]
    }
    return upload(token, metadata, path, publish=publish)
