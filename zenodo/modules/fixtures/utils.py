# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Utils for fixtures."""

from __future__ import absolute_import, print_function

import json

from pkg_resources import resource_stream, resource_string


def read_json(path):
    """Retrieve JSON from package resource."""
    return json.loads(
        resource_string('zenodo.modules.fixtures', path).decode('utf8'))


def file_stream(path):
    """Retrieve JSON from package resource."""
    return resource_stream('zenodo.modules.fixtures', path)
