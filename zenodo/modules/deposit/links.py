# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Deposit links factory."""

from __future__ import absolute_import, print_function

from flask import current_app, request
from invenio_deposit.links import deposit_links_factory
from invenio_uploadbyurl.links import default_uploadbyurl_link_factory


def links_factory(pid, **kwargs):
    """Deposit links factory."""
    links = deposit_links_factory(pid)

    links['html'] = current_app.config['DEPOSIT_UI_ENDPOINT'].format(
        host=request.host,
        scheme=request.scheme,
        pid_value=pid.pid_value,
    )

    ubu_links = default_uploadbyurl_link_factory(pid)
    if ubu_links:
        links.update(ubu_links)

    if current_app.config['ZENODO_ACTIVATE_ROBIS_INTEGRATION']:
        links['approval'] = ('https://www.hzdr.de/db/!WebAppl.Publ_Input.Data'
                             '?pNid=108&pRodareId={0}'.format(pid.pid_value))

    return links
