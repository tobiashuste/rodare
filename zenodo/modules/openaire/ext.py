# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Zenodo OpenAIRE extension."""

from __future__ import absolute_import, print_function

from werkzeug.utils import cached_property

from . import config


class _ZenodoOpenAIREState(object):
    """Store the OpenAIRE mappings."""

    def __init__(self, app):
        self.app = app

    @cached_property
    def openaire_communities(self):
        """Configuration for OpenAIRE communities types."""
        return self.app.config['ZENODO_OPENAIRE_COMMUNITIES']

    @cached_property
    def inverse_openaire_community_map(self):
        """Lookup for Zenodo community -> OpenAIRE community."""
        comm_map = self.openaire_communities
        items = sum([[(z_comm, oa_comm) for z_comm in cfg['communities']] \
            for oa_comm, cfg in comm_map.items()], [])
        ditems = dict(items)
        if len(ditems) < len(items):
            raise ValueError("Communities defined for given OpenAIRE community"
                             " must be unique.")
        return ditems

class ZenodoOpenAIRE(object):
    """Zenodo OpenAIRE extension."""

    def __init__(self, app=None):
        """Extension initialization."""
        if app:
            self.init_app(app)

    @staticmethod
    def init_config(app):
        """Initialize configuration."""
        for k in dir(config):
            if k.startswith('ZENODO_OPENAIRE_'):
                app.config.setdefault(k, getattr(config, k))

    def init_app(self, app):
        """Flask application initialization."""
        self.init_config(app)
        state = _ZenodoOpenAIREState(app)
        self._state = app.extensions['zenodo-openaire'] = state
