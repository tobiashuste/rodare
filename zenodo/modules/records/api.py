# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Record API."""

from __future__ import absolute_import

from os.path import splitext

from flask import request
from invenio_db import db
from invenio_files_rest.models import ObjectVersion
from invenio_pidstore.models import PersistentIdentifier
from invenio_records_files.api import FileObject, FilesIterator, Record, \
    _writable

from .fetchers import zenodo_record_fetcher


class ZenodoFileObject(FileObject):
    """Zenodo file object."""

    def dumps(self):
        """Create a dump."""
        super(ZenodoFileObject, self).dumps()
        self.data.update({
            # Remove dot from extension.
            'type': splitext(self.data['key'])[1][1:].lower(),
            'file_id': str(self.file_id),
        })
        return self.data


class ZenodoFilesIterator(FilesIterator):
    """Zenodo files iterator."""

    @_writable
    def __setitem__(self, key, stream):
        """Add file inside a deposit."""
        with db.session.begin_nested():
            size = None
            if request and request.files and request.files.get('file'):
                size = request.files['file'].content_length or None
            obj = ObjectVersion.create(
                bucket=self.bucket, key=key, stream=stream, size=size)
            self.filesmap[key] = self.file_cls(obj, {}).dumps()
            self.flush()


class ZenodoRecord(Record):
    """Zenodo Record."""

    file_cls = ZenodoFileObject

    files_iter_cls = ZenodoFilesIterator

    record_fetcher = staticmethod(zenodo_record_fetcher)

    @property
    def pid(self):
        """Return an instance of record PID."""
        pid = self.record_fetcher(self.id, self)
        return PersistentIdentifier.get(pid.pid_type, pid.pid_value)

    @property
    def depid(self):
        """Return depid of the record."""
        return PersistentIdentifier.get(
            pid_type='depid',
            pid_value=self.get('_deposit', {}).get('id')
        )
