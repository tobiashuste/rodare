# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Configuration for Zenodo Redirector."""

from __future__ import absolute_import, print_function

ZENODO_TYPE_SUBTYPE_LEGACY = {
    'publications': ('publication', None),
    'books': ('publication', 'book'),
    'books-sections': ('publication', 'section'),
    'conference-papers': ('publication', 'conferencepaper'),
    'journal-articles': ('publication', 'article'),
    'patents': ('publication', 'patent'),
    'preprints': ('publication', 'preprint'),
    'deliverable': ('publication', 'deliverable'),
    'milestone': ('publication', 'milestone'),
    'proposal': ('publication', 'proposal'),
    'reports': ('publication', 'report'),
    'theses': ('publication', 'thesis'),
    'technical-notes': ('publication', 'technicalnote'),
    'working-papers': ('publication', 'workingpaper'),
    'other-publications': ('publication', 'other'),

    'posters': ('poster', None),

    'presentations': ('presentation', None),

    'datasets': ('dataset', None),

    'images': ('image', None),
    'figures': ('image', 'figure'),
    'drawings': ('image', 'drawing'),
    'diagrams': ('image', 'diagram'),
    'photos': ('image', 'photo'),
    'other-images': ('image', 'other'),

    'videos': ('video', None),

    'software': ('software', None),

    'lessons': ('lesson', None),

    'other': ('other', None),
}


#: External redirect URLs
REDIRECTOR_EXTERNAL_REDIRECTS = [
    ['/imprint', 'imprint', 'https://www.hzdr.de/impressum'],
    ['/impressum', 'impressum', 'https://www.hzdr.de/impressum']
]
