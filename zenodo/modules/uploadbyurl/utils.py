# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Customized utility functions of uploadbyurl."""

from __future__ import absolute_import, print_function

import json
import stat

import paramiko
import requests
from flask import current_app, flash


def create_home_directory_fes(server, username, password):
    """Create home directory on FES via Web API."""
    json_req = dict(
        authentication_type='publickey::rsa',
        configuration=dict(
            user=username,
            pwd=current_app.config['UPLOADBYURL_FES_SECRET'],
            public_key='dummy',
        )
    )
    params = {'json': json.dumps(json_req)}
    r = requests.post(
        current_app.config['ZENODO_FES_API'],
        params=params,
    )
    r.raise_for_status()


def deploy_ssh_key_fes(pub_key, server, username, password):
    """Deploy SSH key to remote machine."""
    with paramiko.SSHClient() as client:
        client.load_system_host_keys()
        client.connect(server, username=username, password=password)
        sftp = client.open_sftp()
        home = '/home/' + username
        try:
            sftp.chdir(home)
        except IOError:
            try:
                create_home_directory_fes(server, username, password)
            except requests.exceptions.RequestException:
                flash('Could not create home directory on fes. Please '
                      'contact us.')
                return
        # emulate mkdir -p
        try:
            sftp.chdir(home + '/.ssh')
        except IOError:
            try:
                sftp.mkdir(home + '/.ssh')
            except IOError:
                flash('Could not create .ssh directory. '
                    'Copy the key manually.')
                return
        stdin, stdout, stderr = client.exec_command(
            'echo "%s" >> ~/.ssh/authorized_keys' % pub_key)
        status = stdout.channel.recv_exit_status()
        if status:
            flash('Could not add the SSH public key to authorized_keys. '
                  'Add it manually.')
            return
        # set correct permissions
        try:
            sftp.chmod(home + '/.ssh/authorized_keys',
                stat.S_IRUSR | stat.S_IWUSR)
        except IOError:
            flash('Could not set correct access rights '
                  'for ~/.ssh/authorized_keys.')
            return
        try:
            sftp.chmod(home + '/.ssh/', stat.S_IRWXU)
        except IOError:
            flash('Could not set correct access '
                  'rights for directory ~/.ssh/.')
            return            
