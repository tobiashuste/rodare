# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Custom Sipstore API functionality."""

from flask import has_request_context, request
from flask_login import current_user


def _build_agent_info():
    """Build the SIP agent info.
    This method overwrites the default factory.
    The IP-address should not be stored.
    :returns: Agent information regarding the SIP.
    :rtype: dict
    """
    agent = dict()
    if has_request_context():
        if current_user.is_authenticated and current_user.email:
            agent['email'] = current_user.email
    return agent
