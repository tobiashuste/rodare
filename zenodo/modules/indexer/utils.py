# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Zenodo-specific InvenioIndexer utility functions."""

from __future__ import absolute_import, print_function

from flask import current_app
from invenio_indexer.utils import default_record_to_index


def record_to_index(record):
    """Get the elasticsearch index and doc_type for given record.

    Construct the index name from the `record['$schema']`, which is then
    mapped with an elastisearch document type (fixed difinition in the config).

    :param record: The record object.
    :type record: `invenio_records.api.Record`
    :returns: Tuple of (index, doc_type)
    :rtype: (str, str)
    """
    index, doc_type = default_record_to_index(record)
    return index, current_app.config['INDEXER_SCHEMA_TO_INDEX_MAP'].get(
        index, doc_type)
