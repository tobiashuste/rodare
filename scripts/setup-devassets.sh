#!/usr/bin/env bash
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.
# First install all node modules
CWD=`pwd`
ZENODO_SOURCE=$(dirname $(dirname $(readlink -f $0)))

zenodo npm --pinned-file ${ZENODO_SOURCE}/package.pinned.json
cd ${VIRTUAL_ENV}/var/instance/static
npm install

# Symlink to source directories
cd node_modules
rm -Rf invenio-search-js
ln -s ~/src/invenio-search-js invenio-search-js
rm -Rf invenio-records-js
ln -s ~/src/invenio-records-js invenio-records-js
rm -Rf invenio-files-js
ln -s ~/src/invenio-files-js invenio-files-js

# Make sure they are built
cd ~/src/invenio-search-js
npm install
npm run-script build
cd ~/src/invenio-records-js
npm install
npm run-script build
cd ~/src/invenio-files-js
npm install
npm run-script build

# Build assets
cd ${CWD}
zenodo collect -v
zenodo assets build
