# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Support and contact module for Zenodo."""

from __future__ import absolute_import, print_function

from collections import OrderedDict

from werkzeug.utils import cached_property

from . import config


class ZenodoSupport(object):
    """Zenodo support form."""

    @cached_property
    def categories(self):
        """Return support issue categories."""
        return OrderedDict(
            (c['key'], c) for c in self.app.config['SUPPORT_ISSUE_CATEGORIES'])

    def __init__(self, app=None):
        """Extension initialization."""
        if app:
            self.init_app(app)

    def init_app(self, app):
        """Flask application initialization."""
        self.app = app
        self.init_config(app)
        app.extensions['zenodo-support'] = self

    def init_config(self, app):
        """Flask application initialization."""
        for k in dir(config):
            if k.startswith("SUPPORT_"):
                app.config.setdefault(k, getattr(config, k))
