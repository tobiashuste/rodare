#!/usr/bin/env sh
# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

pydocstyle zenodo tests docs && \
isort -rc -c -df && \
check-manifest --ignore ".travis-*,.ci-*,docs/_build*"
sphinx-build -qnNW docs docs/_build/html && \
pytest tests/unit/default `find tests/unit/ -maxdepth 1 -type d -not -name default` && \
sphinx-build -qnNW -b doctest docs docs/_build/doctest
