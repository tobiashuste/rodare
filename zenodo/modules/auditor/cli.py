# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""CLI for Zenodo-Auditor tasks."""

from __future__ import absolute_import, print_function

import click
from flask.cli import with_appcontext

from .tasks import audit_oai, audit_records


@click.group()
def audit():
    """Zenodo Auditor CLI."""


@audit.command('records')
@click.option('--logfile', '-l', type=click.Path(exists=False, dir_okay=False,
                                                 resolve_path=True))
@click.option('--eager', '-e', is_flag=True)
@with_appcontext
def _audit_records(logfile, eager):
    """Audit all records."""
    if eager:
        audit_records.apply((logfile,), throw=True)
    else:
        audit_records.apply_async((logfile,))


@audit.command('oai')
@click.option('--logfile', '-l', type=click.Path(exists=False, dir_okay=False,
                                                 resolve_path=True))
@click.option('--eager', '-e', is_flag=True)
@with_appcontext
def _audit_oai(logfile, eager):
    """Audit OAI Sets."""
    if eager:
        audit_oai.apply((logfile,), throw=True)
    else:
        audit_oai.apply_async((logfile,))
