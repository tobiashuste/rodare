# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""CLI for Zenodo fixtures."""

from __future__ import absolute_import, print_function

from invenio_db import db
from invenio_migrator.tasks.records import import_record
from invenio_sipstore.models import SIPMetadataType


def loaddemorecords(records):
    """Load demo records."""
    for item in records:
        import_record.delay(item, source_type='json'),


def loadsipmetadatatypes(types):
    """Load SIP metadata types."""
    with db.session.begin_nested():
        for type in types:
            db.session.add(SIPMetadataType(**type))
    db.session.commit()
