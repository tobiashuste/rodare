# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Sanitized Unicode string field."""

from __future__ import absolute_import, print_function

from marshmallow import fields

from .sanitizedunicode import SanitizedUnicode


class SanitizedUrl(SanitizedUnicode, fields.Url):
    """SanitizedString-based URL field."""

    def _deserialize(self, value, attr, data):
        """Deserialize sanitized URL value."""
        # Apply SanitizedUnicode first
        value = SanitizedUnicode._deserialize(self, value, attr, data)
        return fields.Url._deserialize(self, value, attr, data)
