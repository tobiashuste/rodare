# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Zenodo Serializers."""

from __future__ import absolute_import, print_function

import json

from flask import has_request_context
from flask_security import current_user
from invenio_pidrelations.contrib.versioning import PIDVersioning
from invenio_records_files.api import Record
from invenio_records_rest.serializers.json import JSONSerializer

from zenodo.modules.records.serializers.pidrelations import \
    serialize_related_identifiers

from ..permissions import has_read_files_permission


class ZenodoJSONSerializer(JSONSerializer):
    """Zenodo JSON serializer.

    Adds or removes files from depending on access rights and provides a
    context to the request serializer.
    """

    def preprocess_record(self, pid, record, links_factory=None):
        """Include files for single record retrievals."""
        result = super(ZenodoJSONSerializer, self).preprocess_record(
            pid, record, links_factory=links_factory
        )
        # Add/remove files depending on access right.
        if isinstance(record, Record) and '_files' in record:
            if not has_request_context() or has_read_files_permission(
                    current_user, record):
                result['files'] = record['_files']

        # Serialize PID versioning as related identifiers
        pv = PIDVersioning(child=pid)
        if pv.exists:
            rels = serialize_related_identifiers(pid)
            if rels:
                result['metadata'].setdefault(
                    'related_identifiers', []).extend(rels)
        return result

    def preprocess_search_hit(self, pid, record_hit, links_factory=None,
                              **kwargs):
        """Prepare a record hit from Elasticsearch for serialization."""
        result = super(ZenodoJSONSerializer, self).preprocess_search_hit(
            pid, record_hit, links_factory=links_factory, **kwargs
        )
        # Add files if in search hit (only public files exists in index)
        if '_files' in record_hit['_source']:
            result['files'] = record_hit['_source']['_files']
        elif '_files' in record_hit:
            result['files'] = record_hit['_files']
        return result

    def dump(self, obj, context=None):
        """Serialize object with schema."""
        return self.schema_class(context=context).dump(obj).data

    def transform_record(self, pid, record, links_factory=None):
        """Transform record into an intermediate representation."""
        return self.dump(
            self.preprocess_record(pid, record, links_factory=links_factory),
            context={'pid': pid}
        )

    def transform_search_hit(self, pid, record_hit, links_factory=None):
        """Transform search result hit into an intermediate representation."""
        return self.dump(
            self.preprocess_search_hit(
                pid, record_hit, links_factory=links_factory),
            context={'pid': pid}
        )

    def serialize_exporter(self, pid, record):
        """Serialize a single record for the exporter."""
        return json.dumps(
            self.transform_search_hit(pid, record)
        ).encode('utf8')  + b'\n'
