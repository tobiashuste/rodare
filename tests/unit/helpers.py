# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Helpers."""

from __future__ import absolute_import, print_function, unicode_literals

from contextlib import contextmanager
from copy import deepcopy

from flask import current_app

from zenodo.modules.deposit.api import ZenodoDeposit as Deposit


def bearer_auth(headers, token):
    """Create authentication headers (with a valid oauth2 token)."""
    headers = deepcopy(headers)
    headers.append(
        ('Authorization', 'Bearer {0}'.format(token['token'].access_token))
    )
    return headers


def login_user_via_session(client, user=None, email=None):
    """Login a user via the session."""
    if not user:
        user = current_app.extensions['security'].datastore.find_user(
            email=email)
    with client.session_transaction() as sess:
        sess['user_id'] = user.get_id()


def publish_and_expunge(db, deposit):
    """Publish the deposit and expunge the session.

    Use this if you want to be safe that session is synced with the DB after
    the deposit publishing.
    """
    deposit.publish()
    dep_uuid = deposit.id
    db.session.commit()
    db.session.expunge_all()
    deposit = Deposit.get_record(dep_uuid)
    return deposit


@contextmanager
def recaptcha_enabled(app):
    """Temporarily enable recaptcha."""
    orig_public_key = app.config.get('RECAPTCHA_PUBLIC_KEY')
    orig_private_key = app.config.get('RECAPTCHA_PRIVATE_KEY')
    app.config['RECAPTCHA_PUBLIC_KEY'] = 'test-key'
    app.config['RECAPTCHA_PRIVATE_KEY'] = 'test-key'
    yield
    app.config['RECAPTCHA_PUBLIC_KEY'] = orig_public_key
    app.config['RECAPTCHA_PRIVATE_KEY'] = orig_private_key
